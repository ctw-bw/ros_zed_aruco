# More info on ZED docker, see:
# https://hub.docker.com/r/stereolabs/zed
# https://github.com/stereolabs/zed-docker/blob/master/3.X/ubuntu/runtime/Dockerfile

# More info on ROS docker and ROS install, see:
# https://devanshdhrafani.github.io/blog/2021/04/15/dockerros2.html
# https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html
# https://answers.ros.org/question/384705/cmake-fails-with-no-cmake_cxx_compiler-could-be-found-docker-melodic-ros-core/
# https://github.com/osrf/docker_images/blob/master/ros2/source/devel/Dockerfile

# More info on communication with ROS2 from inside a container
# https://stackoverflow.com/questions/65900201/troubles-communicating-with-ros2-node-in-docker-container

# Container must run with --privileged for camera & network access
# Container must run with --gpus all for Nvidia driver access on host


ARG UBUNTU_RELEASE_YEAR
ARG CUDA_MAJOR
ARG CUDA_MINOR
ARG CUDA_PATCH

FROM nvidia/cuda:${CUDA_MAJOR}.${CUDA_MINOR}.${CUDA_PATCH}-base-ubuntu${UBUNTU_RELEASE_YEAR}.04

ARG UBUNTU_RELEASE_YEAR
ARG CUDA_MAJOR
ARG CUDA_MINOR
ARG ZED_SDK_MAJOR
ARG ZED_SDK_MINOR

ARG ROS_DISTRO

ARG PYZED_WRAPPER_TAG
ARG ROS_PACKAGE_TAG
ARG NAKAMA_EVE_INTERFACES_TAG

# NVIDIA for ZED
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}compute,video,utility,graphics

RUN echo "Europe/Paris" > /etc/localtime ; echo "CUDA Version ${CUDA_MAJOR}.${CUDA_MINOR}.0" > /usr/local/cuda/version.txt


# Git, Nano, and set locale to US english UTF8 for ROS2
RUN apt-get update && apt-get install -y \
        git \
        nano \
        tzdata \
        locales \ 
    && locale-gen en_US.UTF-8 \
    && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/*
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8


# ZED SDK
RUN apt-get update -y && apt-get upgrade -y && apt-get autoremove -y \
    && apt-get install --no-install-recommends -y \
        lsb-release \ 
        wget \
        less \ 
        udev \
        sudo \
    && wget -q -O ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run https://download.stereolabs.com/zedsdk/${ZED_SDK_MAJOR}.${ZED_SDK_MINOR}/cu${CUDA_MAJOR}${CUDA_MINOR%.*}/ubuntu${UBUNTU_RELEASE_YEAR} \
    && chmod +x ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run ; ./ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run -- silent runtime_only \
    && rm ZED_SDK_Linux_Ubuntu${UBUNTU_RELEASE_YEAR}.run \
    && rm -rf /var/lib/apt/lists/*


# ZED Python API, with (headless) contrib version of OpenCV for Aruco
RUN apt-get update -y && apt-get install --no-install-recommends -y \
        python3 \
        python3-pip \ 
        libpng-dev \
        libgomp1 \
    && python3 -m pip install --upgrade pip \  
    && wget download.stereolabs.com/zedsdk/pyzed -O /usr/local/zed/get_python_api.py \
    && python3 /usr/local/zed/get_python_api.py \
    && python3 -m pip install \
        numpy \
        opencv-contrib-python-headless \
        *.whl \
    && rm *.whl ; rm -rf /var/lib/apt/lists/*


# ROS2 Foxy base
RUN apt-get update -y && apt-get install -y \
        curl \
        gnupg2 \
        lsb-release \
    && curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null \    
    && apt-get update -y && apt-get install -y \
        ros-foxy-ros-base \
        build-essential \
        python3-argcomplete \
        python3-rosdep \
        python3-colcon-common-extensions \
        python3-colcon-mixin \
#        pyton3-vcstool \
    && rm -rf /var/lib/apt/lists/*
    
# ROS2 Foxy environment variables
ENV ROS_DISTRO=foxy
ENV AMENT_PREFIX_PATH=/opt/ros/foxy
ENV COLCON_PREFIX_PATH=/opt/ros/foxy
ENV LD_LIBRARY_PATH=/opt/ros/foxy/lib
ENV PATH=/opt/ros/foxy/bin:$PATH
# ENV PYTHONPATH=/opt/ros/foxy/lib/python3.8/site-packages
ENV ROS_PYTHON_VERSION=3
ENV ROS_VERSION=2
ENV ROS_DOMAIN_ID=4
ENV DEBIAN_FRONTEND=

# Replace shared memory use by network communication
COPY ./fastrtps_default_profiles.xml /.
ENV FASTRTPS_DEFAULT_PROFILES_FILE='/fastrtps_default_profiles.xml'

# Nakama Pyzed Wrapper from bitbucket
# https://pip.pypa.io/en/stable/topics/vcs-support/
RUN python3 -m pip install -e "git+https://bitbucket.org/ctw-bw/pyzed_wrapper.git@$PYZED_WRAPPER_TAG#egg=pyzed_wrapper"

# Create ROS workspace, set rosdep, pull and build packages
RUN mkdir -p /root/ros_ws/src/
WORKDIR /root/ros_ws/src/
RUN git clone -b $ROS_PACKAGE_TAG https://bitbucket.org/ctw-bw/ros_zed_aruco.git \
    && git clone -b $NAKAMA_EVE_INTERFACES_TAG https://bitbucket.org/ctw-bw/nakama_eve_interfaces.git \
    && rosdep init && rosdep update && rosdep install -i --from-path . --rosdistro foxy -y

WORKDIR /root/ros_ws/
RUN /bin/bash -c "source /opt/ros/foxy/setup.bash && colcon build" 


# Entrypoint with ROS2 sourcing
COPY ./entrypoint.sh /.
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["bash"]

