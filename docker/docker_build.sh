#!/bin/bash

# Build settings
UBUNTU_RELEASE_YEAR=20
CUDA_MAJOR=11
CUDA_MINOR=0
CUDA_PATCH=3
ZED_SDK_MAJOR=3
ZED_SDK_MINOR=7
ROS_DISTRO='foxy'
PYZED_WRAPPER_TAG='release-0.0.4'
ROS_PACKAGE_TAG='release-0.0.3'
NAKAMA_EVE_INTERFACES_TAG='release-0.0.1'

docker build \
    --build-arg UBUNTU_RELEASE_YEAR=$UBUNTU_RELEASE_YEAR \
    --build-arg CUDA_MAJOR=$CUDA_MAJOR \
    --build-arg CUDA_MINOR=$CUDA_MINOR \
    --build-arg CUDA_PATCH=$CUDA_PATCH \
    --build-arg ZED_SDK_MAJOR=$ZED_SDK_MAJOR \
    --build-arg ZED_SDK_MINOR=$ZED_SDK_MINOR \
    --build-arg ROS_DISTRO=$ROS_DISTRO \
    --build-arg PYZED_WRAPPER_TAG=$PYZED_WRAPPER_TAG \
    --build-arg ROS_PACKAGE_TAG=$ROS_PACKAGE_TAG \
    --build-arg NAKAMA_EVE_INTERFACES_TAG=$NAKAMA_EVE_INTERFACES_TAG \
    -t "be1et/nakama-eve:ros2-zed-aruco-${ROS_PACKAGE_TAG}" .
