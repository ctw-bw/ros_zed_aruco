#!/bin/bash
set -e

source "/opt/ros/foxy/setup.bash"
source "./install/setup.bash"

ros2 run ros_zed_aruco ros_zed_aruco_publisher

exec "$@"
