"""
ROS2 publisher node for Aruco data, obtained from ZED camera.

TODO: currently only the LEFT camera is used for estimation
"""

import rclpy
from rclpy.node import Node
from nakama_eve_interfaces.msg import ArucoData
from ros_zed_aruco.aruco import ArucoWrapper
import numpy as np
import pyzed_wrapper as pw
from pyzed_wrapper import helper as pw_h
from ros_zed_aruco.settings import Settings


class Publisher(Node):

    def __init__(self):
        super().__init__('publisher')
        self.publisher_ = self.create_publisher(ArucoData, 'aruco_data', 10)
        return


    def publish_message(self, message):
        self.publisher_.publish(message)
        return


def main(args=None):

    rclpy.init(args=args)

    settings = Settings()

    # Camera interface and intrinsics
    zed = pw.Wrapper(settings.ZED_INPUT_MODE)
    zed.image_retrieval_parameters.view = pw.sl.VIEW.LEFT
    zed.open_input_source()
    intrinsic_left, intrinsic_right = zed.get_intrinsic()
    intrinsic_matrix_left = pw_h.to_intrinsic_matrix(intrinsic_left)

    # Marker interface
    aruco = ArucoWrapper(
        settings.ARUCO_MARKER_SIZE,
        settings.ARUCO_MARKER_TOTAL,
        settings.ARUCO_PHYSICAL_MARKER_SIZE)

    # Drive publisher by camera output rate
    publisher = Publisher()
    while True:

        # Wait for camera frame and apply Aruco detection
        zed.retrieve()
        if zed.output_image.shape[2] > 3:
            aruco.detect_markers(zed.output_image[:, :, 0:3])
        else:
            aruco.detect_markers(zed.output_image)
        aruco.estimate_pose(intrinsic_matrix_left, intrinsic_left.disto)

        # Create and publish ROS2 message
        message = ArucoData()
        if aruco.marker_ids is not None:
            message.marker_ids = aruco.marker_ids.ravel().astype('int').tolist()
            message.bounding_boxes = np.vstack(aruco.bounding_boxes).ravel().astype('int').tolist()
            message.translation_vectors = aruco.translation_vectors
            message.rotation_vectors = aruco.rotation_vectors
        publisher.publish_message(message)



if __name__ == "__main__":
    main()
