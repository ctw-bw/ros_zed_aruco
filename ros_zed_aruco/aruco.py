"""
Assumes cv2 version 4.7.X
"""

import numpy as np
import cv2
import cv2.aruco as aruco


class ArucoWrapper(object):

    def __init__(self, marker_size=4, marker_total=50, marker_physical_size=0.1):
        """
        =INPUT=
            marker_size - int
                Aruco marker size in dictionary, either 4, 5, 6, or 7
            marker_total - int
                Aruco dictionary size, either 50, 10, 250, or 1000
            marker_physical_size - float
                Aruco marker physical size, in meters
        =NOTES=
            Also see: https://chev.me/arucogen/
            Because marker_physical_size is set once, it is assumed all markers
            have the same physical dimensions
        """

        self.marker_physical_size = marker_physical_size
        self.object_points = self.marker_physical_size * np.array([
            [-0.5, 0.5, 0],
            [0.5, 0.5, 0],
            [0.5, -0.5, 0],
            [-0.5, -0.5, 0]], dtype="float32")

        self.dictionary_key = f'DICT_{marker_size}X{marker_size}_{marker_total}'
        if self.dictionary_key not in dir(aruco):
            raise Exception(f'Dictionary key {self.dictionary_key} not available.')
        self.dictionary = aruco.getPredefinedDictionary(getattr(aruco, self.dictionary_key))
        self.detector_parameters = aruco.DetectorParameters()
        self.detector = aruco.ArucoDetector(self.dictionary, self.detector_parameters)

        # Detection information. Can contain info from multiple markers
        self.bounding_boxes = None
        self.marker_ids = None
        self.reject = None
        self.rotation_vectors = None
        self.translation_vectors = None
        return


    def detect_markers(self, image):
        """
        =INPUT=
            image - OpenCV compatible image array, such as ndarray
        """

        self.bounding_boxes, self.marker_ids, self.reject = (
            self.detector.detectMarkers(image))

        return


    def estimate_pose(self, intrinsic_calibration, distortion_coefficients):
        """
        =INPUT=
            intrinsic_calibration - ndarray (3, 3), float
                Of the form [[fx, 0, cx], [0, fy, cy], [0, 0, 1]]
            distortion_coefficients - ndarray (N, ), float
                Where N is 4, 5, 8, or 12, depending on how many distortion
                coefficients should be used. Of the form:
                k_1, k_2, p_1, p_2[, k_3[, k_4, k_5, k_6],[s_1, s_2, s_3, s_4]]
        =NOTES=
            The marker corrdinate system is centered on the middle of the
            marker, with the Z axis perpendicular to the marker plane.
        """
        
        self.rotation_vectors = []
        self.translation_vectors = []
        if self.marker_ids is None:
            return

        for bounding_box in self.bounding_boxes:
            is_success, rvec, tvec = cv2.solvePnP(
                    self.object_points,
                    bounding_box,
                    intrinsic_calibration,
                    distortion_coefficients)
            
            if is_success:
                self.rotation_vectors += rvec.ravel().tolist()
                self.translation_vectors += tvec.ravel().tolist()
            else:
                self.rotation_vectors += [0, 0, 0]
                self.translation_vectors += [0, 0, 0]

        return
