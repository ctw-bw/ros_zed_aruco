
import os

# Check for environmental variables, passed to Docker on runtime

class Settings:
    ZED_INPUT_MODE = "stream"
    ARUCO_MARKER_SIZE = 5
    ARUCO_PHYSICAL_MARKER_SIZE = 0.1
    ARUCO_MARKER_TOTAL = 50

    def __init__(self):
    
        # Replace default with environ setting, if any
        for attr in dir(Settings):
            if "_" not in attr[0] and attr in os.environ:
                value = os.environ[attr]
                if value.isdigit():
                    value = int(value)
                elif value.replace(".", "", 1).isdigit():
                    value = float(value)
                setattr(Settings, attr, value)
        return

