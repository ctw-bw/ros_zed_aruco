from setuptools import setup

package_name = 'ros_zed_aruco'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='mark',
    maintainer_email='m.vlutters@utwente.nl',
    description='Put ARUCO marker pose on the ROS2 network, using ZED camera',
    license='Apache2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "ros_zed_aruco_publisher = ros_zed_aruco.publisher:main"
        ],
    },
)
