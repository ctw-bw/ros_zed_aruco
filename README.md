
# Ros zed aruco
ROS package to consume the camera stream from a ZED camera, run the images through the OpenCV Aruco library to do bounding box and pose estimation, and then put the information on the ROS network.

## Dependencies
* ROS2 Foxy
* ZED2 Python SDK version 3.6 (pyzed.sl)
* ZED2 Python SDK wrapper (pyzed wrapper)
* OpenCV-contrib 4 Aruco library
* Nvidia driver

## Docker
It is suggested to use the docker container available from hub.docker.com. 

### Dependencies for docker container
* The above dependencies are already installed in the docker container. Software needed to run the container are:
    * Docker engine
    * Nvidia driver
    * nvidia-container-toolkit 
        * https://www.stereolabs.com/docs/docker/install-guide-linux/

### Pulling the container from dockerhub
$ docker pull be1et/nakama-eve:ros2-zed-aruco-release-0.0.2

### Running the container
* For proper functioning of the image acquisition and the access of ROS, 
it is required to run with the following flags:
    * $ docker run --gpus all -it --privileged --net=host image_tag:container_tag

* It is possible to adjust runtime settings through additional environmental
variables passed at runtime. Defaults are given below (when not passed):
    * -e ZED_INPUT_MODE="stream"
    * -e ARUCO_MARKER_SIZE=5
    * -e ARUCO_MARKER_TOTAL=50
    * -e ARUCO_PHYSICAL_MARKER_SIZE=0.1

### Building container
* Use the docker_build.sh for building and modifying build parameters.
    * $ bash docker_build.sh


